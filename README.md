# Resources

[Node.js API](http://nodejs.org/api/)

[express.js API](http://expressjs.com/api.html)

# Setup Node.js
```
 sudo add-apt-repository ppa:chris-lea/node.js
 sudo apt-get update
 sudo apt-get install nodejs
```

# Install Dependencies
```
 npm install
```

# Run
```
 node app/server.js
```
