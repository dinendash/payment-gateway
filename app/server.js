"use strict";

var nodemailer = require('nodemailer'),
    Firebase = require('firebase'),
    express = require('express'),
    _ = require('underscore'),
    stripe = require('stripe')('sk_test_Tr64z9uK3lvDka9WyVXTkPWM'),
    fs = require('fs'),
    app = express(),
    port = 3001,
    mailer = nodemailer.createTransport('SMTP', {}),
    dollar_round = function (amount) {return (Math.round(amount * 100) / 100).toFixed(2)},
    template, email_template;

fs.readFile( __dirname + '/email.html', function (err, data) {
    if (err) throw err;
    email_template = _.template(data.toString());
});

app.use(express.json());

app.post('/payment/card/:table_id/:payment_id', function(req, res) {
    var payment_id = req.params.payment_id,
        table_id = req.params.table_id,
        token = req.body.token,
        subtotal = req.body.subtotal,
        tax = req.body.tax,
        tip = req.body.tip,
        total = req.body.total,
        card_text = token.card.type + ' (**** **** **** ' + token.card.last4 + ')',
        table_entry = new Firebase('https://dinedash.firebaseIO.com/tables/' + table_id),
        table_payment = new Firebase('https://dinedash.firebaseIO.com/tables/' + table_id + '/payments/' + payment_id);

    console.log('Received data for payment: ' + payment_id);
    console.log('Processing: Payment for ' + card_text);
    table_entry.once('value', function (data) {
        var table = data.val();
        stripe.charges.create({
            amount: parseInt(total*100, 10),
            currency: "cad",
            card: token.id,
            description: "Mahony and Sons Meal (" + payment_id + ")"
        }).then(function (result) {
            var payment = new Firebase('https://dinedash.firebaseIO.com/payments/' + payment_id),
                payment_data = {
                    completed: true,
                    email: token.email,
                    created: result.created * 1000,
                    card_type: token.card.type,
                    card_number: token.card.last4,
                    subtotal: subtotal,
                    tax: tax,
                    tip: tip,
                    total: total,
                    items: _.where(table.items, {selected: payment_id}),
                };
            payment.set(payment_data);
            console.log('Approved: Payment for ' + card_text);
            table_payment.set({
                subtotal: subtotal,
                tax: tax,
                tip: tip,
                total: total,
                email: token.email,
            });
            res.send({status: 'success'});
        }, function (err) {
            console.log(err);
            console.log('Failed: Payment for ' + card_text);
            res.status(400).send({status: 'failed'});
        });
    }, function () {
        console.log('Failed: Payment for ' + card_text);
        res.status(400).send({status: 'failed'});
    });
});

app.post('/payment/email/:payment_id', function(req, res) {
    var payment_id = req.params.payment_id,
        payment_entry = new Firebase('https://dinedash.firebaseIO.com/payments/' + payment_id),
        failed = function () {
            console.log('Payment not found. Failed to send email to: ' + req.body.email_address);
            res.status(400).send({status: 'failed'});
        },
        email_address = req.body.email_address;

    console.log('Received request for email: ' + payment_id);
    console.log('Sending email to: ' + email_address);
    payment_entry.once('value', function (data) {
        var payment = data.val(),
            message,
            email_payment = {};
        if (!payment) {
            failed();
            return
        }
        email_payment.total = dollar_round(payment.total);
        email_payment.subtotal = dollar_round(payment.subtotal);
        email_payment.tax = dollar_round(payment.tax);
        email_payment.tip = dollar_round(payment.tip);
        _.each(payment.items, function (item) {
            item.price = dollar_round(item.price);
        });
        message = email_template({
            items: payment.items,
            name: payment.name,
            card_number: payment.card_number,
            card_type: payment.card_type,
            payment: email_payment,
            created: (new Date(payment.created)).toISOString().replace('-', '/').replace('-', '/').replace('T', ' ').substring(0, 16),
        });
        mailer.sendMail({
            from: "Dine 'N' Dash <receipt@dinendash.in>",
            to: req.body.email_address,
            subject: "Thanks for dining with at Mahony and Sons, here is your receipt.",
            html: message,
            generateTextFromHTML: true,
        }, function (error) {
            if (error) {
                console.log('Failed to send email to: ' + email_address);
                res.status(400).send({status: 'failed'});
            } else {
                console.log('Sent email to: ' + email_address);
                payment_entry.child('email_sent_to').set(email_address);
                payment_entry.child('email_sent').set(true);
                res.send({status: 'sent'});
            }
        });
    }, failed);
});

app.listen(port);
console.log('Server running on port ' + port);
